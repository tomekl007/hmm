package dist.test;

import dist.DiscreteDistribution;
import dist.Distribution;
import dist.hmm.ForwardBackwardProbabilityCalculator;
import dist.hmm.HiddenMarkovModelReestimator;
import dist.hmm.SimpleHiddenMarkovModel;
import dist.hmm.StateSequenceCalculator;
import shared.DataSet;
import shared.Instance;

public class HMMUserWantToBuyPrediction {
    /**
     * The main method
     *
     * @param args ignored
     */

    private final static int LOGIN = 0;
    private final static int SHOW_PRODUCT = 1;
    private final static int BUY = 2;

    public static void main(String[] args) {


        // simple coin flip test
        SimpleHiddenMarkovModel model = new SimpleHiddenMarkovModel(3);

        model.setInitialStateProbabilities(new double[]{
                .6, .35, .05 //initial probability that is LOGIN is 60%, show SHOW_PRODUCT 35, and BUY 5%
        });

        model.setOutputDistributions(new Distribution[]{
                new DiscreteDistribution(new double[]{.0, .95, .05}),
                new DiscreteDistribution(new double[]{.2, .65, .15}),
                new DiscreteDistribution(new double[]{.0, .98, .02})
        });

        model.setTransitionProbabilities(new double[][]{
                {.0, .95, .05}, //probability of transition from LOGIN to LOGIN is 0%, from LOGIN to SHOW_PRODUCT 95% FROM LOGIN to BUY 5%
                {.2, .65, .15}, //probability of transition from SHOW_PRODUCT to LOGIN is 20%, from SHOW_PRODUCT to SHOW_PRODUCT 65%, from SHOW_PRODUCT to BUY 15%
                {.0, .98, .02}  //probability of transition from BUY to LOGIN is 0%, from BUY to SHOW_PRODUCT 98, from BUY to BUY 2%
        });


        //we want to find a probability that user LOGIN, SHOW_PRODUCT and BUY
        DataSet sequenceToFindProbabilityOf = new DataSet(new Instance[]{
                new Instance(LOGIN), new Instance(SHOW_PRODUCT), new Instance(BUY)
        });


        //observations
        DataSet[] observations = new DataSet[]{
                sequenceToFindProbabilityOf,
                new DataSet(new Instance[]{
                        new Instance(LOGIN), new Instance(SHOW_PRODUCT), new Instance(SHOW_PRODUCT)
                }),
                new DataSet(new Instance[]{
                        new Instance(SHOW_PRODUCT), new Instance(LOGIN), new Instance(SHOW_PRODUCT)
                }),
                new DataSet(new Instance[]{
                        new Instance(LOGIN), new Instance(SHOW_PRODUCT), new Instance(BUY)
                })
        };
        System.out.println(model + "\n");
        printObservations(observations);

        ForwardBackwardProbabilityCalculator fbc =
                new ForwardBackwardProbabilityCalculator(model, sequenceToFindProbabilityOf);

        findProbabilityOfSearchingSequence(fbc);

        StateSequenceCalculator vc =
                new StateSequenceCalculator(model, sequenceToFindProbabilityOf);
        int[] states = vc.calculateStateSequence();
        System.out.println("Most likely state sequence of first sequence: ");
        for (int i = 0; i < states.length; i++) {
            System.out.print(states[i] + " ");
        }
        System.out.println();
        System.out.println();
        System.out.println("Reestimations of model based on observations: ");
        HiddenMarkovModelReestimator bwr = new HiddenMarkovModelReestimator(model, observations);
        bwr.train();
        System.out.println(model + "\n");
        bwr.train();
        System.out.println(model + "\n");
        for (int i = 0; i < 1000; i++) {
            bwr.train();
        }
        System.out.println(model + "\n");
        fbc = new ForwardBackwardProbabilityCalculator(model, sequenceToFindProbabilityOf);
        System.out.println("Probability of first sequenceToFindProbabilityOf: ");
        System.out.println(fbc.calculateProbability());
        System.out.println("Probabilities of other observations: ");
        for (int i = 1; i < observations.length; i++) {
            fbc = new ForwardBackwardProbabilityCalculator(model, observations[i]);
            System.out.println(fbc.calculateProbability());
        }

    }

    private static void findProbabilityOfSearchingSequence(ForwardBackwardProbabilityCalculator fbc) {

        System.out.println("Probability of first sequenceToFindProbabilityOf: ");
        System.out.println(fbc.calculateProbability());
        System.out.println();
    }

    private static void printObservations(DataSet[] observations) {
        System.out.println("Observation Sequences: ");
        for (int i = 0; i < observations.length; i++) {
            System.out.println(observations[i]);
        }
        System.out.println();
    }
}
