package dist.test;

import dist.DiscreteDistribution;
import dist.Distribution;
import dist.hmm.ForwardBackwardProbabilityCalculator;
import dist.hmm.HiddenMarkovModelReestimator;
import dist.hmm.SimpleHiddenMarkovModel;
import dist.hmm.StateSequenceCalculator;
import shared.DataSet;
import shared.Instance;

public class HMMWeatherForecast {
    /**
     * The main method
     *
     * @param args ignored
     */

    private final static int SUNNY = 0;
    private final static int RAINY = 1;
    private final static int FOGGY = 2;

    public static void main(String[] args) {


        // simple coin flip test
        SimpleHiddenMarkovModel model = new SimpleHiddenMarkovModel(3);

        model.setInitialStateProbabilities(new double[]{
                .6, .3, .1 //initial probability that is sunny is 60%, rainy 30%, and foggy 10%
        });

        model.setOutputDistributions(new Distribution[]{
                new DiscreteDistribution(new double[]{.8, .05, .15}),
                new DiscreteDistribution(new double[]{.2, .6, .2}),
                new DiscreteDistribution(new double[]{.2, .3, .5})
        });

        model.setTransitionProbabilities(new double[][]{
                {.8, .05, .15}, //probability of transition from SUNNY to SUNNY is 80%, from SUNNY to RAINY 5% ...
                {.2, .6, .2},   //probability of transition from RAINY to SUNNY is 20%, ....
                {.2, .3, .5}    //probability of transition from FOGGY to SUNNY is 20%, ....
        });


        //we want to find a probability that three SUNNY days in a row will happen
        DataSet sequenceToFindProbabilityOf = new DataSet(new Instance[]{
                new Instance(SUNNY), new Instance(SUNNY), new Instance(SUNNY)
        });


        //observations
        DataSet[] observations = new DataSet[]{
                sequenceToFindProbabilityOf,
                new DataSet(new Instance[]{
                        new Instance(SUNNY), new Instance(RAINY), new Instance(FOGGY)
                }),
                new DataSet(new Instance[]{
                        new Instance(RAINY), new Instance(RAINY), new Instance(RAINY)
                }),
                new DataSet(new Instance[]{
                        new Instance(FOGGY), new Instance(SUNNY), new Instance(SUNNY)
                })
        };
        System.out.println(model + "\n");
        printObservations(observations);

        ForwardBackwardProbabilityCalculator fbc =
                new ForwardBackwardProbabilityCalculator(model, sequenceToFindProbabilityOf);

        findProbabilityOfSearchingSequence(fbc);

        StateSequenceCalculator vc =
                new StateSequenceCalculator(model, sequenceToFindProbabilityOf);
        int[] states = vc.calculateStateSequence();
        System.out.println("Most likely state sequence of first sequence: ");
        for (int i = 0; i < states.length; i++) {
            System.out.print(states[i] + " ");
        }
        System.out.println();
        System.out.println();
        System.out.println("Reestimations of model based on observations: ");
        HiddenMarkovModelReestimator bwr = new HiddenMarkovModelReestimator(model, observations);
        bwr.train();
        System.out.println(model + "\n");
        bwr.train();
        System.out.println(model + "\n");
        for (int i = 0; i < 1000; i++) {
            bwr.train();
        }
        System.out.println(model + "\n");
        fbc = new ForwardBackwardProbabilityCalculator(model, sequenceToFindProbabilityOf);
        System.out.println("Probability of first sequenceToFindProbabilityOf: ");
        System.out.println(fbc.calculateProbability());
        System.out.println("Probabilities of other observations: ");
        for (int i = 1; i < observations.length; i++) {
            fbc = new ForwardBackwardProbabilityCalculator(model, observations[i]);
            System.out.println(fbc.calculateProbability());
        }

    }

    private static void findProbabilityOfSearchingSequence(ForwardBackwardProbabilityCalculator fbc) {

        System.out.println("Probability of first sequenceToFindProbabilityOf: ");
        System.out.println(fbc.calculateProbability());
        System.out.println();
    }

    private static void printObservations(DataSet[] observations) {
        System.out.println("Observation Sequences: ");
        for (int i = 0; i < observations.length; i++) {
            System.out.println(observations[i]);
        }
        System.out.println();
    }

}
